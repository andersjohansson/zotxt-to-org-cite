;;; zotxt-to-org-cite.el --- Functions for converting org-zotxt citations to org-cite  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Anders Johansson

;; Author: Anders Johansson <anders.l.johansson@chalmers.se>
;; Keywords: wp
;; Created: 2021-09-01
;; Modified: 2022-11-09

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A few functions for transforming org-zotxt citations (with my
;; additions in zotxt-extra) into the new org-cite-format.
;;
;; See also: https://gitlab.com/andersjohansson/emacs-zotxt-extra

;;; Code:

(require 'org)
(require 'org-zotxt)
(require 'zotxt-extra)
(require 'org-element)

;;;###autoload
(defun zotxt-to-org-cite-at-point (arg)
  "Transform zotxt link at point to cite.
Updates citekeys from zotero unless prefix ARG is given."
  (interactive "P")
  (unless arg
    (let ((zotxt--debug-sync t)) ;; needs to be synchronous, done first
      (zotxt-extra-update-reference-link-at-point)))
  (when (org-in-regexp org-link-bracket-re)
    (zotxt-to-org-cite--transform-matched)))

;;;###autoload;
(defun zotxt-to-org-cite (&optional arg)
  "Transform zotxt links to cite in buffer or region if active.
Updates citekeys from zotero unless prefix ARG is given."
  (interactive "P")
  (unless arg
    (let ((zotxt--debug-sync t)) ;; needs to be synchronous, done first
      (zotxt-extra-update-all-reference-links)))
  (save-excursion
    (save-restriction
      (when (use-region-p)
        (narrow-to-region (region-beginning) (region-end)))
      (goto-char (point-max))
      (while (re-search-backward org-link-bracket-re nil t)
        (zotxt-to-org-cite--transform-matched))
      (zotxt-to-org-cite-merge-consecutive-citations))))

(defun zotxt-to-org-cite--transform-matched ()
  "Transforms link at point matched by ‘org-link-bracket-re’."
  (when (equal "zotero" (car (save-match-data
                               (split-string (match-string-no-properties 1) ":"))))
    (if-let ((desc (match-string-no-properties 2))
             (rep-match-data (match-data))
             (lp (zotxt-extra-link-parse-desc
                  (replace-regexp-in-string
                   "\\\\_" "_"
                   desc))))
        (unless (zotxt-extra--no-specials-check
                 "" lp #'always :nocite :textcite :nptextcite :citeauthor
                 :Citeauthor :citefullauthor :suppress-author)
          (let* ((cit
                  `(citation
                    ( :style ,(cond
                               ((plist-get lp :nocite) "nocite")
                               ((plist-get lp :textcite) "text")
                               ((plist-get lp :nptextcite) "text/bare")
                               ((plist-get lp :citeauthor) "author")
                               ((plist-get lp :Citeauthor) "author/caps-full")
                               ((plist-get lp :citefullauthor) "author/full")
                               ((plist-get lp :suppress-author) "noauthor")))))
                 (cref
                  `(citation-reference
                    ( :prefix ,(plist-get lp :prefix)
                      :suffix ,(let ((lw (plist-get lp :locator-word))
                                     (lo (plist-get lp :locator))
                                     (su (plist-get lp :suffix)))
                                 (if lo
                                     (concat
                                      " " lw " " lo
                                      (when su (concat ", " su)))
                                   su))
                      :key ,(plist-get lp :citekey))))
                 (rep
                  (org-element-citation-interpreter
                   cit
                   (org-element-citation-reference-interpreter cref nil))))
            ;; (message rep)
            (set-match-data rep-match-data)
            (replace-match rep t t)
            (set-match-data rep-match-data t)))
      (display-warning 'zotxt
                       (format "Failed parsing citation %s"
                               (match-string 0)))
      "(FAILED PARSING CITATION)")))

;;;###autoload
(defun zotxt-to-org-cite-merge-consecutive-citations ()
  "Merge consecutive org-cite citations.
Only works for the simplest cases. Replaces in current visible
part of current buffer."
  (interactive)
  (let* ((re (format "\\(?1:%s\\)\\(?2:%s\\)]"
                     org-element-citation-prefix-re
                     org-element-citation-key-re))
         (allowedbetween "[[:space:]]*")
         ;; (wrap)
         (delim "; ")
         (nextre (concat allowedbetween re)))
    (goto-char (point-min))
    (while (search-forward-regexp re nil t)
      (let ((citelist (list (match-string 2)))
            (point-at-first (match-beginning 0))
            (start-style (match-string 1)))
        (while (looking-at nextre)
          (push (match-string 2) citelist)
          (goto-char (match-end 0)))
        (delete-region point-at-first (match-end 0))
        (goto-char point-at-first)
        (insert (format
                 (concat
                  start-style
                  "%s"
                  "]")
                 (string-join (nreverse citelist) delim)))))))


(provide 'zotxt-to-org-cite)
;;; zotxt-to-org-cite.el ends here
